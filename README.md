## Page of my Flipstarter development work

> [Check my website for more info](https://salemkode.com)
---

### [Add a field for progress tracking URL](https://gitlab.com/flipstarter/backend/-/merge_requests/56)

add a field for the Flipstarter creator to specify where people can track progress.

---
### [refactor create page](https://gitlab.com/flipstarter/backend/-/merge_requests/53)

most changes were made to the internal structure of the page to facilitate future additions

---

### [use express-sse 0.5.1](https://gitlab.com/flipstarter/backend/-/merge_requests/55)

Got this error after submitting

---

### [make social media pick up the summary](https://gitlab.com/flipstarter/backend/-/merge_requests/51)

Got this error after submitting

---

### [update all dependencies](https://gitlab.com/flipstarter/backend/-/merge_requests/50)

Optimizing and updating dependencies, handling and checking problems after update

---

### [Add language dynamically](https://gitlab.com/flipstarter/backend/-/merge_requests/48)

However it's troublesome to add a new language so I thought I can improve the code to make it easy and smooth to add more languages by having a json file like this:

---

## donations

if you like my work donate to this address

    bitcoincash:qrl5ttrc2rgwqt4lsr09n33mk529fevjwyg3us9xkd
    

<img src="https://gitlab.com/salemkode/donation/-/raw/main/Image.svg?inline=false" />
